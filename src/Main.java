import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException, RestriccionVioladaException {
		TendidoElectrico tendido = new TendidoElectrico();//instanciamos objeto tendido
		File archivo = new File("Archivos/ciudades.in");//abrimos archivo
		Scanner sc = new Scanner(archivo);//iniciamos escritura
		int cantidad_vertices = sc.nextInt();//leemos cantidad de vertices
		sc.close();//cerramos archivo
		Grafos m0 = new Grafos(cantidad_vertices);//instanciamos un objeto grafo el cual toma como parametro la cantidad de vertices
		m0.setMatAdyPeso(tendido.leerArchivo("Archivos/ciudades.in"));//leemos y establecemos la matriz de adyacencia con peso del archivo
		tendido.algoritmo(m0, tendido.centrales);//iniciamos el algoritmo que resuelve el ejercicio pasando como parametros el grafo y el vector conn las centrales
	}
	
}
