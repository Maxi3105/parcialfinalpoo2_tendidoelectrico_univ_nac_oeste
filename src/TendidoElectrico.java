import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class TendidoElectrico {

	protected int[] centrales;
	private ArrayList<Integer> cant_centrales;
	private int costos = 0;
	private ArrayList<Arista> lista_vertice = new ArrayList<Arista>();
	
	
	//metodo de lectura de archivo al que le paso una direccion
	
	public int[][] leerArchivo(String direccion) throws FileNotFoundException, RestriccionVioladaException {
		
		File archivo = new File(direccion);//iniciamos la lectura
		Scanner sc = new Scanner(archivo);//empezamos la lectura
		int cantidad_vertices = sc.nextInt();//leemos cantidad de vertices necesarios para instanciar objeto grafo
		if(cantidad_vertices>100 | cantidad_vertices <0 ) {
			throw new RestriccionVioladaException("no cumple con la condicion");
		}//restriccion exception
		Grafos matriz = new Grafos(cantidad_vertices);//creamos objeto grafo pasandole como parametro la cantidad de vertices
		int cantidad_centrales = sc.nextInt();//leemos la cantidad de centrales
		if(cantidad_centrales>100 | cantidad_centrales <0 ) {
			throw new RestriccionVioladaException("no cumple con la condicion");
		}//restriccion exception
		cant_centrales = new ArrayList<Integer>();
		this.centrales = new int[cantidad_centrales];
		
		for (int z = 0; z < cantidad_centrales; z++) {
			int dato=sc.nextInt();//dato representa numero de vertice que representa una central
			if(this.centrales[z]!=dato)//verifica que el vector de centrales este vacio
				this.centrales[z] = dato;//asigna a cada posicion el numero de vertice que tiene la central
				this.cant_centrales.add(dato);//guarda en la lista el numero de vertice que tiene central
			}
		
		//cargamos la matriz de adyacencia ponderada
		for (int i = 0; i < cantidad_vertices; i++) {
			for (int j = 0; j < cantidad_vertices; j++) {
				int valor=sc.nextInt();
				if(i==j && valor!=0) {
					throw new RestriccionVioladaException("no cumple con la condicion");
				}//restriccion exception
				matriz.agregarPesoArista2(i, j, valor);

			}
		}
		sc.close();//cierro archivo
		return matriz.getMatAdyPeso();	//retorno matriz ponderada
	}
	
	//algoritmo similar a prim que realiza el arbol de recubrimiento minimo entre centrales
	//pasamos como parametro el grafo y el vector que contiene los vertices que son centrales
	
	public void algoritmo(Grafos grafo, int[] centrales) throws IOException{
		
		int grado = grafo.getVertsVec().length;//averiguo grado o cantidad de vertices
		boolean[] visitado = new boolean[grado];//instancio un vector booleano del tama�o del vetor de vertices
		int fila = 0, columna = 0, fila_minima = 0, columna_minima = 0, minimo_actual = -1, minimo_anterior ;
		
		//relleno el vector booleano visitado con false los vertices que son ciudades y true los que son centrales
		inicializar_vector(visitado, centrales);
		//pasa como parametro vector booleano visitado y comprueba de que no esten todos visitados
		
		if (!todosVisitados(visitado)) {
			for (fila = 0; fila < grado ; fila++) {
				if (!visitado[fila]) {//verifica que el recorrido no sea desde una central, osea vertice false
					minimo_actual = -1;//instancia minimo actual
					 minimo_anterior = -1;//instancia minimo anterior
					for (columna = 0; columna < grado; columna++) {//recorre la columna de cada fila(recorre matriz)
						
						//si no hay bucle, si tiene peso y si uno de los dos vertices es una central 
						
						
						if (fila != columna && grafo.getPeso(fila, columna) != 0 && this.estaenCentrales(columna+1)) {
							//si minimo es -1 asigna nuevo valor (peso arista)
							if (minimo_actual == -1)
								minimo_actual = grafo.getPeso(fila, columna);
							else
								//saca el minimo entre un vertice y otro
								
								minimo_actual = Math.min(minimo_actual, grafo.getPeso(fila, columna));
							//si el minimo actual es distinto anterior entonces se le asigna:
							//a minimo anterior el valor minimo actual
							//a fila minima el valor fila +1
							//a columna minima valor columna +1
							
							if (minimo_actual != minimo_anterior) {
								minimo_anterior = minimo_actual;
								fila_minima = fila+1;
								columna_minima = columna+1;
							}
						} else;
						
					}
					//guarda en la lista la arista y el costo en el  acumulador de costo
					if (minimo_actual != -1) {
						lista_vertice.add(new Arista((fila_minima), (columna_minima)));
						this.costos += minimo_actual;
				
					}
					visitado[fila] = true;//asigna al vertice como visitado
				}
			}
		}
		this.escribirArchivo();//escribe resultado en el archivo
	}
	
	public boolean estaenCentrales(int columna) {
		//pasamos por parametro la columna+1 y nos devuelve true si esta en el vector de centrales
		//para que no se realice el arbol entre ciudades
		int i;
		for(i=0;i<this.centrales.length;i++) {
			if(columna==centrales[i]) {
				return true;
			}
			
		}return false;
		
	}
	
	//inicia el vector booleano visitados con false en las ciudades y true en las centrales
	private void inicializar_vector(boolean[] visitado, int[] centrales) {
		for (int i = 0; i < visitado.length; i++) {
			visitado[i] = false;
		}
		for (int j = 0; j < centrales.length; j++) {
		visitado[centrales[j] - 1] = true;
	}
	}
//corroboramos que las ciudades esten en false y las centrales en true
	private boolean todosVisitados(boolean[] incluido) {
		for (boolean r : incluido) {
			if (!r)
				return r;
		}
		return true;
	}
	
	//escribe archivo
	public void escribirArchivo() throws IOException {
		
		PrintWriter pw = new PrintWriter(new FileWriter("Archivos/tendido.out"));
		pw.println(this.costos);
		for (int i = 0; i < this.lista_vertice.size(); i++) {
			if (this.lista_vertice.get(i).getVertice1() < this.lista_vertice.get(i).getVertice2()) {
				pw.println(this.lista_vertice.get(i).getVertice1() + " " + this.lista_vertice.get(i).getVertice2());
			} else {
				pw.println(this.lista_vertice.get(i).getVertice2() + " " + this.lista_vertice.get(i).getVertice1());
			}
		}
		pw.close();

	
}
	
}
